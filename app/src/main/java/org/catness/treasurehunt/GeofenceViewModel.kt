package org.catness.treasurehunt

import android.location.Location
import android.util.Log
import androidx.lifecycle.*

private const val GEOFENCE_INDEX_KEY = "geofenceIndex"
private const val GEOFENCE_STATUS_KEY = "geofenceStatus"

enum class GeofenceModelStatus {
    INACTIVE,
    ACTIVE,
    FOUND,
    GAMEOVER
}

class GeofenceViewModel(state: SavedStateHandle) : ViewModel() {
    private val LOG_TAG: String = this.javaClass.simpleName

    private var savedState = state
    var geofenceIndex: Int = state.get(GEOFENCE_INDEX_KEY) ?: -1

    var status: GeofenceModelStatus = state.get(GEOFENCE_STATUS_KEY) ?: GeofenceModelStatus.INACTIVE

    private var distance: Float = 999999f
    private val _trackerString = MutableLiveData<String>()
    val trackerString: LiveData<String>
        get() = _trackerString

    private val _progress = MutableLiveData<Int>(0)
    val progress: LiveData<Int>
        get() = _progress

    private val _showTracker = MutableLiveData<Boolean>(false)
    val showTracker: LiveData<Boolean>
        get() = _showTracker

    private var _trackerClicked = MutableLiveData<Boolean>(false)
    val trackerClicked: LiveData<Boolean>
        get() = _trackerClicked

    private var _locationFound = MutableLiveData<Boolean>(false)
    val locationFound: LiveData<Boolean>
        get() = _locationFound

    init {
        _trackerString.value = when (status) {
            GeofenceModelStatus.FOUND -> "Location found: " + GeofencingConstants.LANDMARK_DATA[geofenceIndex!!].id
            GeofenceModelStatus.GAMEOVER -> "Game over, all locations found"
            else -> "..."
        }
    }

    fun updateDistance(location: Location) {
        if (status != GeofenceModelStatus.ACTIVE) return
        distance = location.distanceTo(GeofencingConstants.LANDMARK_DATA[geofenceIndex].location)

        Log.i(
            LOG_TAG,
            "distance = $distance" +
                    " src= ${location.latitude},${location.longitude} " +
                    " target=" + GeofencingConstants.LANDMARK_DATA[geofenceIndex].location.latitude +
                    ", " + GeofencingConstants.LANDMARK_DATA[geofenceIndex].location.longitude
        )

            //   _trackerString.value = distance.toString() + " " + GeofencingConstants.LANDMARK_DATA[geofenceIndex!!].id
        if (distance < GeofencingConstants.GEOFENCE_RADIUS_IN_METERS) locationFound()
        else {
            if (distance > 1000f + GeofencingConstants.GEOFENCE_RADIUS_IN_METERS) _progress.value = 0
            else _progress.value = ((1000f - distance + GeofencingConstants.GEOFENCE_RADIUS_IN_METERS) / 10f).toInt()
            // Log.i(LOG_TAG,"distance = $distance progress value = ${_progress.value} ")
        }
    }

    fun locationFound() {
        status = GeofenceModelStatus.FOUND
        _trackerString.value =
            "Location found: " + GeofencingConstants.LANDMARK_DATA[geofenceIndex].id
        savedState.set(GEOFENCE_STATUS_KEY, status)
        _locationFound.value = true
        _showTracker.value = false
    }

    fun activateNextLocation() {
        if (status == GeofenceModelStatus.ACTIVE) return
        if (geofenceIndex < GeofencingConstants.NUM_LANDMARKS - 1) {
            geofenceIndex++
            status = GeofenceModelStatus.ACTIVE
            _showTracker.value = true
            Log.i(LOG_TAG, "activate location: index=$geofenceIndex")
        } else {
            gameOver()
        }
        savedState.set(GEOFENCE_INDEX_KEY, geofenceIndex)
    }

    fun gameOver() {
        status = GeofenceModelStatus.GAMEOVER
        _trackerString.value = "Congratulations, you found everything!"
        _showTracker.value = false
    }

    fun onClickTracker() {
        when (status) {
            GeofenceModelStatus.FOUND -> {
                activateNextLocation()
                if (status == GeofenceModelStatus.GAMEOVER) return;
                // will show the message and require another click to reset
            }
            GeofenceModelStatus.GAMEOVER -> {
                geofenceIndex = -1
                activateNextLocation()
            }
            else -> return
        }
        Log.i(LOG_TAG,"activated next location; status = $status")
        _trackerClicked.value = true
        // goes to the main activity to activate the next geofence
    }

    fun endClickTracker() {
        Log.i(LOG_TAG, "End click tracker")
        _trackerClicked.value = false
    }

    fun endLocationFound() {
        _locationFound.value = false
    }

}