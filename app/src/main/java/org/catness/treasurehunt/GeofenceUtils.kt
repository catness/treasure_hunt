package org.catness.treasurehunt

import android.content.Context
import android.location.Location
import android.location.LocationManager
import com.google.android.gms.location.GeofenceStatusCodes
import com.google.android.gms.maps.model.LatLng

/**
 * Returns the error string for a geofencing error code.
 */
fun errorMessage(context: Context, errorCode: Int): String {
    val resources = context.resources
    return when (errorCode) {
        GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE -> resources.getString(
            R.string.geofence_not_available
        )
        GeofenceStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES -> resources.getString(
            R.string.geofence_too_many_geofences
        )
        GeofenceStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS -> resources.getString(
            R.string.geofence_too_many_pending_intents
        )
        else -> resources.getString(R.string.unknown_geofence_error)
    }
}

/**
 * Stores latitude and longitude information along with a hint to help user find the location.
 */
data class LandmarkDataObject(val id: String, val latLong: LatLng, val location: Location)

internal object GeofencingConstants {
    /**
     * Used to set an expiration time for a geofence. After this amount of time, Location services
     * stops tracking the geofence. For this sample, geofences expire after one hour.
     */
    val GEOFENCE_EXPIRATION_IN_MILLISECONDS: Long = java.util.concurrent.TimeUnit.HOURS.toMillis(1)

    // Hong Kong: Lantau Island

    val LANDMARK_DATA = arrayOf(
        LandmarkDataObject(
            "Ngong Ping 360",
            LatLng(22.2562512,113.9008458),
            Location(LocationManager.NETWORK_PROVIDER)
        ),
        LandmarkDataObject(
            "Tian Tan Buddha",
            LatLng(22.2539775,113.9039007),
            Location(LocationManager.NETWORK_PROVIDER)
        ),
        LandmarkDataObject(
            "Po Lin Monastery",
            LatLng(22.2554762,113.9076488),
            Location(LocationManager.NETWORK_PROVIDER)
        )
    )

    val NUM_LANDMARKS = LANDMARK_DATA.size
    const val GEOFENCE_RADIUS_IN_METERS = 100f
    const val EXTRA_GEOFENCE_INDEX = "GEOFENCE_INDEX"

    init {
        LANDMARK_DATA.forEach {
            it.location.latitude = it.latLong.latitude
            it.location.longitude = it.latLong.longitude
        }
    }

}
