package org.catness.treasurehunt

import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.SavedStateViewModelFactory
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import org.catness.treasurehunt.databinding.ActivityMapsBinding

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, LocationSource {
    private val LOG_TAG: String = this.javaClass.simpleName
    private lateinit var map: GoogleMap

    private var latOff = 0.0
    private var lngOff = 0.0
    private var latReal = 0.0
    private var lngReal = 0.0
    private var zoomLevel = 15f
    private var trackLocation = true

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 999
        internal const val ACTION_GEOFENCE_EVENT =
            "MapsActivity.treasureHunt.action.ACTION_GEOFENCE_EVENT"
    }

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var mapLocationListener: LocationSource.OnLocationChangedListener? = null
    private var marker: Marker? = null

    private lateinit var tracker: TextView
    private lateinit var viewModel: GeofenceViewModel
    private lateinit var binding: ActivityMapsBinding
    private lateinit var geofencingClient: GeofencingClient
    private lateinit var receiver: GeofenceBroadcastReceiver

    private var locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            super.onLocationResult(locationResult)
            var locations: MutableList<Location> = locationResult.locations
            locations.forEach {
                latReal = it.latitude
                lngReal = it.longitude
                viewModel.updateDistance(it)
                it.latitude += latOff
                it.longitude += lngOff
                // Log.i(LOG_TAG, "lat lng = ${it.latitude} ${it.longitude} real = $latReal $lngReal")
                // Log.i(LOG_TAG, "offsets: latOff=$latOff lngOff=$lngOff")
                mapLocationListener?.onLocationChanged(it)
                updateMarker(it)
            }
        }
    }

    private fun configureReceiver() {
        Log.i(LOG_TAG, "configure receiver")
        val filter = IntentFilter()
        filter.addAction("org.catness.treasurehunt")
        receiver = GeofenceBroadcastReceiver()
        registerReceiver(receiver, filter)
    }

    // A PendingIntent for the Broadcast Receiver that handles geofence transitions.
    private lateinit var geofencePendingIntent: PendingIntent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        tracker = binding.tracker

        viewModel = ViewModelProviders.of(
            this, SavedStateViewModelFactory(
                this.application,
                this
            )
        ).get(GeofenceViewModel::class.java)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this

        configureReceiver()

        geofencingClient = LocationServices.getGeofencingClient(this)

        // Observer for the tracker clicker
        viewModel.trackerClicked.observe(this, Observer { it ->
            if (it) {
                Log.i(LOG_TAG, "tracker clicked ! status=${viewModel.status}")
                viewModel.endClickTracker()
                if (viewModel.status == GeofenceModelStatus.ACTIVE) addGeofence()
            }
        })

        viewModel.locationFound.observe(this, Observer { it ->
            if (it) {
                Log.i(LOG_TAG, "location found!")
                viewModel.endLocationFound()
            }
        })


        val intent = Intent(this, GeofenceBroadcastReceiver::class.java)
        intent.action = ACTION_GEOFENCE_EVENT
        // Use FLAG_UPDATE_CURRENT so that you get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        geofencePendingIntent =
            PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        // Create channel for notifications
        createChannel(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.getUiSettings().setZoomControlsEnabled(true)
        // map.setOnMarkerClickListener(this)
        map.setLocationSource(this)

        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Log.i(LOG_TAG, "request permissions from onMapReady")
            PermissionUtils.requestPermission(
                    this,
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            return
        }

        map.isMyLocationEnabled = true

    }

    @SuppressLint("MissingPermission")
    private fun setUpLocationListener() {

        Log.i(LOG_TAG, "set up location listener")

        val fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        // for getting the current location update after every 2 seconds with high accuracy
        val locationRequest = LocationRequest().setInterval(2000).setFastestInterval(2000)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.myLooper()
        )

        addGeofence()
    }

    private fun moveMarker(latLng: LatLng) {
        map.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        marker?.setPosition(latLng)
    }

    private fun updateMarker(location: Location) {
        var latLng = LatLng(location.latitude, location.longitude)

        if (marker == null) {
            Log.i(LOG_TAG, "create marker zoom=$zoomLevel")
            marker = map.addMarker(
                MarkerOptions().position(latLng).title("I am here")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
            )
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomLevel))
        } else {
            if (trackLocation) {
                moveMarker(latLng)
            }
        }
    }


    /*
  *  When the user clicks on the notification, this method will be called, letting us know that
  *  the geofence has been triggered, and it's time to move to the next one in the treasure
  *  hunt.
  */
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        val extras = intent?.extras
        if (extras != null) {
            if (extras.containsKey(GeofencingConstants.EXTRA_GEOFENCE_INDEX)) {
                viewModel.locationFound()
            }
        }
    }

    override fun onStart() {
        Log.i(LOG_TAG, "state: start")
        super.onStart()
        when {
            PermissionUtils.isPermissionGranted(this) -> {
                Log.i(LOG_TAG, "permissions already granted")
                when {
                    PermissionUtils.isLocationEnabled(this) -> {
                        setUpLocationListener()

                    }
                    else -> {
                        PermissionUtils.showGPSNotEnabledDialog(this)
                    }
                }
            }
            else -> {
                Log.i(LOG_TAG, "request permissions from onStart")

                PermissionUtils.requestPermission(
                    this,
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        Log.i(LOG_TAG, "on request permissions result: code=$requestCode")
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isEmpty()) {
                    Log.i(LOG_TAG, "grantresults is empty")
                } else {
                    Log.i(
                        LOG_TAG,
                        "grantresults=" + grantResults[0].toString() + " " + PackageManager.PERMISSION_DENIED.toString()
                    )
                }
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionUtils.isLocationEnabled(this) -> {
                            setUpLocationListener()
                            map.isMyLocationEnabled = true
                        }
                        else -> {
                            PermissionUtils.showGPSNotEnabledDialog(this)
                        }
                    }
                } else {
                    Log.i(LOG_TAG, "permission not granted :(")
                    Toast.makeText(
                        this,
                        getString(R.string.location_permission_not_granted),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    override fun activate(p0: LocationSource.OnLocationChangedListener?) {
        Log.i(LOG_TAG, "state: activate")
        mapLocationListener = p0

    }

    override fun deactivate() {
        Log.i(LOG_TAG, "state: deactivate")
        mapLocationListener = null
    }

    override fun onStop() {
        Log.i(LOG_TAG, "state: stop")
        super.onStop()
        //     LocationServices.getFusedLocationProviderClient(this)
        //         .removeLocationUpdates(locationCallback)

    }

    override fun onPause() {
        Log.i(LOG_TAG, "state: pause")
        super.onPause()
        //   LocationServices.getFusedLocationProviderClient(this)
        //       .removeLocationUpdates(locationCallback)
    }

    override fun onResume() {
        Log.i(LOG_TAG, "state: resume")
        super.onResume()
        //   setUpLocationListener()
    }

    override fun onDestroy() {
        Log.i(LOG_TAG, "state: destroy")
        super.onDestroy()
        LocationServices.getFusedLocationProviderClient(this)
            .removeLocationUpdates(locationCallback)
        unregisterReceiver(receiver)
    }

    @SuppressLint("MissingPermission")
    private fun addGeofence() {
        Log.i(LOG_TAG, "addGeoFence ***********")
        viewModel.activateNextLocation() // update the geofence index - or game over
        if (viewModel.status == GeofenceModelStatus.GAMEOVER) return

        val currentGeofenceData = GeofencingConstants.LANDMARK_DATA[viewModel.geofenceIndex]
        // Build the Geofence Object
        val geofence = Geofence.Builder()
            // Set the request ID, string to identify the geofence.
            .setRequestId(currentGeofenceData.id)
            // Set the circular region of this geofence.
            .setCircularRegion(
                currentGeofenceData.latLong.latitude,
                currentGeofenceData.latLong.longitude,
                GeofencingConstants.GEOFENCE_RADIUS_IN_METERS
            )
            // Set the expiration duration of the geofence. This geofence gets
            // automatically removed after this period of time.
            .setExpirationDuration(GeofencingConstants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)
            // Set the transition types of interest. Alerts are only generated for these
            // transition. We track entry and exit transitions in this sample.
            .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
            .build()

        // Build the geofence request
        val geofencingRequest = GeofencingRequest.Builder()
            // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
            // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
            // is already inside that geofence.
            .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)

            // Add the geofences to be monitored by geofencing service.
            .addGeofence(geofence)
            .build()

        // First, remove any existing geofences that use our pending intent
        geofencingClient.removeGeofences(geofencePendingIntent)?.run {
            // Regardless of success/failure of the removal, add the new geofence
            addOnCompleteListener {
                // Add the new geofence request with the new geofence
                geofencingClient.addGeofences(geofencingRequest, geofencePendingIntent)?.run {
                    addOnSuccessListener {
                        // Geofences added.
                        Toast.makeText(
                            this@MapsActivity, R.string.geofences_added,
                            Toast.LENGTH_SHORT
                        )
                            .show()
                        Log.i(LOG_TAG, "Geofence added: " + geofence.requestId.toString())

                    }
                    addOnFailureListener {
                        // Failed to add geofences.
                        Log.i(LOG_TAG, "geofence not added")
                        Toast.makeText(
                            this@MapsActivity, R.string.geofences_not_added,
                            Toast.LENGTH_SHORT
                        ).show()
                        if ((it.message != null)) {
                            Log.i(LOG_TAG, "geofence error message: " + it.message.toString())
                        }
                    }
                }
            }
        }
    }


}