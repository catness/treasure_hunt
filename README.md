# About the project

**TreasureHunt** is an example for [Advanced Android in Kotlin 04.2: Google Maps](https://codelabs.developers.google.com/codelabs/advanced-android-kotlin-training-geofencing). The user has to walk around and look for several predefined locations with the use of a tracker, which displays the percentage of how close the user is to the current "treasure" location. When the location is found, click on the message to start tracking the next location, etc. When all the locations are found, it starts from the beginning, for debugging purposes.

Tracking starts from 1km, so the user should be that close to all the treasures, otherwise there will be no clues in which direction to go. If it were a real app, then the server would check the user's location and send one of the closest treasures for the geofence.

In any case, an app like that doesn't really need geofences, because the tracker makes very little sense without the app being on the foreground. Plus, I use the Fused Provider to get explicit access to location, and in this case, background location is only updated a couple of times per hour. (I don't remember where the info comes from.) So it's just playing around with the geofence code. 

Also, it doesn't work with Android 11, because it's not possible anymore to ask for foreground and background location permissions at once (see https://developer.android.com/about/versions/11/privacy/location). This was not mentioned in the codelab... 

No apk, just in case my API key gets too much activity. Also, the screenshots are from the emulator, out of privacy concerns. The user is supposed to be on Lantau island, Hong Kong, in the neighbourhood of Tian Tan Buddha.
___

My blog: [Cat's Mysterious Box](http://catness.org/logbook/)
